<?php

/**
 * @file
 * Hooks provided by the Deploy Extra module.
 */

/**
 * Allow modules to modify an entity before it will be imported.
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 *
 * @param $uuid
 *   UUID of the entity.
 *
 * @param $entity
 *   The entity being imported.
 *
 * @param $config
 *   The array of deployment settings.
 */
function hook_deploy_extra_import_entity_alter ($entity_type, $uuid, &$entity, &$config) {

}

/**
 *  Allow modules to modify a lift of import callbacks.
 *
 * @param $callbacks
 *  The list of import callbacks.
 *
 * @param $config
 *   The array of deployment settings.
 */
function hook_deploy_extra_import_callbacks (&$callbacks, $config) {

}

/**
 *  Allow modules to do something after the deployment process has been finished.
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 *
 * @param $uuid
 *   UUID of the entity.
 *
 * @param $entity
 *   The entity being imported.
 *
 * @param $config
 *   The array of deployment settings.
 *
 * @param $status
 *   The status of entity deployment.
 */
function hook_deploy_extra_import_complete ($entity_type, $entity, $uuid, $config, $status) {

}
