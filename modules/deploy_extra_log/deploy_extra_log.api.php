<?php

/**
 * @file
 * Hooks provided by the Deploy Extra Log module.
 */

/**
 * Allow modules to check of structure of imported entities.
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 *
 * @param $entity
 *   The entity being imported.
 *
 * @return $messages
 *   An array of messages as follows:
 * @code
 *   $messages = array(
 *     '0' => array(
 *       'type' => t('General'),
 *       'message' => t('This entity type - node does not exist on the site.'),
 *     ),
 *   );
 * @endcode
 */
function hook_deploy_extra_check_structure ($entity_type, $entity) {

}

/**
 * Allow modules to modify messages.
 *
 * @param $messages
 *  An array of messages returned from hook_deploy_extra_check_structure()
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 *
 * @param $entity
 *   The entity being imported.
 */
function hook_deploy_extra_check_structure_alter (&$messages, $entity_type, $entity) {

}

/**
 * Allow modules to check of structure of fields of imported entities.
 *
 * @param $entity_type
 *  The entity type; e.g. 'node' or 'user'.
 *
 * @param $entity
 *  The entity being imported.
 *
 * @param $field
 *  The field structure.
 *
 * @param $instance
 *  The instance structure.
 *
 * @param $langcode
 *  The language code of the field
 *
 * @param $items
 *  An array of field items keyed by delta
 *
 * @return $messages
 *   An array of messages as follows:
 * @code
 *   $messages = array(
 *     '0' => array(
 *       'type' => t('General'),
 *       'message' => t('This entity type - node does not exist on the site.'),
 *     ),
 *   );
 * @endcode
 */
function hook_deploy_extra_check_field_structure ($entity_type, $entity, $field, $instance, $langcode, $items) {

}

/**
 * Allow modules to alter checking of structure of fields of imported entities.
 *
 * @param $messages
 *   An array of messages
 *
 * @param $entity_type
 *  The entity type; e.g. 'node' or 'user'.
 *
 * @param $entity
 *  The entity being imported.
 *
 * @param $field
 *  The field structure.
 *
 * @param $instance
 *  The instance structure.
 *
 * @param $langcode
 *  The language code of the field
 *
 * @param $items
 *  An array of field items keyed by delta
 */

function hook_deploy_extra_check_field_structure_alter (&$messages, $entity_type, $entity, $field, $instance, $langcode, $items) {

}
