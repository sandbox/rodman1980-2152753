api = 2
core = 7.x

; Modules
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"
