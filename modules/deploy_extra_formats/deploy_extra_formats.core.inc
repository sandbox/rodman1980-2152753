<?php

/**
 * @file
 * This is the file description for Deploy Extra Formats core.
 */

/**
 * Return plugins configuration.
 *
 * @param null $id
 *   Format ID (DeployExtraJSON, DeployExtraYaml & etc.)
 *
 * @return array
 */
function deploy_extra_formats_get_formats_plugins($id = NULL) {
  ctools_include('plugins');
  return ctools_get_plugins('deploy_extra_formats', 'formats', $id);
}

/**
 * Return export formats options.
 */
function deploy_extra_formats_get_formats_options($form_state, $method) {
  // Get plugins.
  $formats = deploy_extra_formats_get_formats_plugins();
  $options = array();

  // Fill the options array.
  if (!empty($formats)) {
    foreach($formats as $format) {
      $options['plugins_formats'][$format['handler']['class']] = $format['name'];

      // Initialize the plugin.
      $plugin = new $format['handler']['class'];

      // Collect the plugins form configurations.
      $options['plugins_forms'] = $plugin->$method($form_state);
    }
  }

  return $options;
}

/**
 * Return methods options.
 *
 * @param $label
 *   Export or import label.
 *
 * @return array
 */
function deploy_extra_formats_get_methods_options($label) {
  $methods = array();
  foreach (module_invoke_all('deploy_extra_formats_methods') as $method => $array) {
    if ($array[$label] != 'none') {
      $methods[$method] = $array[$label];
    }
  }

  return $methods;
}

/**
 * Validation handler for export|import formats.
 *   Uses in deploy_extra_export module and in deploy_extra_import module.
 */
function deploy_extra_formats_validate_formats($element, &$form_state) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_FORMATS_TRANSLATE_CONTEXT);

  if ($element['#value'] == 'DeployExtraYaml') {
    // Spyc library need for export|import data in YAML format.
    $library = libraries_detect('spyc');
    if (empty($library['installed'])) {
      form_error($element, t('You need to download the !spyc, extract the archive and place in the %path directory on your server.', array(
          '!spyc' => l(t('Spyc library'), $library['download url']),
          '%path' => 'sites/all/libraries'
        ), $context)
      );
    }
  }
}

/**
 * Decode|encode data in necessary format.
 *
 * @param $data
 *   Data for encoding|decoding.
 *
 * @param $format
 *   Format of data(DeployExtraJSON, DeployExtraYaml).
 *
 * @param $action
 *   Available params: encodeData, decodeData.
 *
 * @return mixed
 */
function deploy_extra_formats_convert_data($data, $format, $action) {
  // Load formatter and convert (encode|decode) the data.
  $formatter = deploy_extra_formats_get_formats_plugins($format);
  $plugin = new $formatter['handler']['class'];

  return $plugin->$action($data);
}