<?php

/**
 * @file
 * This is the file description for Deploy Extra Import batch.
 */

/**
 * Batch finish import operation.
 *
 * @param $success
 *   Status of batch.
 *
 * @param $results
 *   Batch results.
 *
 * @param $operations
 *   Batch operations.
 */
function deploy_extra_import_batch_finished_operation($success, $results, $operations) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_IMPORT_TRANSLATE_CONTEXT);

  $status  = $success ? 'status' : 'error';
  $message = $success ? 'Import of entities has done successfully!' : 'Import of entities has failed';

  // Send the message.
  drupal_set_message(t($message, array(), $context), $status);
}

/**
 * Configure options of batch.
 *
 * @param $data
 *   Data to be processed.
 *
 * @param $import_function
 *   Import function which will be called in batch.
 *
 * @param array $batch_options
 *   Batch options(title, finished, etc.) will be added to configured batch.
 *
 * @return array
 *   Configured batch.
 */
function deploy_extra_import_configure_batch($data, $import_function, $batch_options = array()) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_IMPORT_TRANSLATE_CONTEXT);

  // Initialize operations array.
  $operations = array();

  if (!empty($data['entities'])) {
    foreach ($data['entities'] as $entity) {
      // Add deployment key to configuration array.
      $import_config['deployment_key'] = $entity['__metadata']['deploy_extra']['deployment_key'];

      // Array of input arguments for import operation.
      $options = array(
        $entity['__metadata']['type'],
        $entity['uuid'],
        $entity,
        $import_config,
      );

      // Add import operation and pass the arguments to it.
      $operations[] = array($import_function, $options);
    }
  }

  $batch = array(
    'title' => t('Importing...', array(), $context),
    'operations' => $operations,
  );

  return array_merge($batch, $batch_options);
}
