<?php

/**
 * @file
 * This is the file description for Deploy Extra Import core.
 */

/**
 * Function used in installation profile for import entities from file.
 *   The callback defined in hook_install_tasks must call this function.
 *
 * @param $file
 *   Full path to file.
 *
 * @param $format
 *   Imported format(DeployExtraJSON, DeployExtraYaml).
 *
 * @return array
 *   Array of batch options.
 *
 * @see deploy_extra_formats_convert_data()
 * @see deploy_extra_import_configure_batch()
 */
function deploy_extra_import_entities_callback($file, $format) {
  if (file_exists($file) && !empty($format)) {
    $data = file_get_contents($file);
    $decoded = deploy_extra_formats_convert_data($data, $format, 'decodeData');

    return deploy_extra_import_configure_batch($decoded, 'deploy_extra_import_import_entity');
  }
}

/**
 * Import function when deploy_extra_import_entity is not exist.
 *   Also used in deploy_extra_import_entities_callback().
 *
 * @see entity_uuid_save()
 */
function deploy_extra_import_import_entity($entity_type, $uuid, $entity, $config) {
  try {
    $controller = entity_get_controller($entity_type);
    if ($controller instanceof EntityAPIControllerInterface) {
      $entity = $controller->create($entity);
    }
    else {
      $entity = (object) $entity;
    }
    entity_uuid_save($entity_type, $entity);
    return $entity;
  } catch (Exception $exception) {
    watchdog_exception('deploy_extra_import', $exception);
  }
}
