<?php

/**
 * @file
 * Includes the admin callbacks and functions for Deploy Extra Import module.
 */

/**
 * Interface form of import.
 */
function deploy_extra_import_entity_import_form($form, &$form_state) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_IMPORT_TRANSLATE_CONTEXT);

  // Fieldset.
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import interface', array(), $context),
  );

  // Get import formats options.
  $options = deploy_extra_formats_get_formats_options($form_state, 'configImportForm');

  // List of import formats.
  $form['import']['format'] = array(
    '#type' => 'radios',
    '#title' => t('Choose import formats', array(), $context),
    '#options' => $options['plugins_formats'],
    '#default_value' => 'DeployExtraJSON',
    '#required' => TRUE,
    '#weight' => 1,
    '#element_validate' => array('deploy_extra_formats_validate_formats'),
  );

  // Add config plugins forms.
  $form['import']['plugins_config'] = $options['plugins_forms'];

  // Get import methods options.
  $methods = deploy_extra_formats_get_methods_options('import_label');

  // List of import methods.
  $form['import']['method'] = array(
    '#type' => 'radios',
    '#title' => t('Choose mode', array(), $context),
    '#options' => $methods,
    '#default_value' => 'form',
    '#required' => TRUE,
    '#weight' => 2,
  );

  // Import text area form.
  $form['import']['form'] = array(
    '#type' => 'textarea',
    '#title' => t('Paste you code here', array(), $context),
    '#default_value' => '',
    '#states' => array(
      'enabled' => array(
        ':input[name="method"]' => array(
          'value' => 'form',
        ),
      ),
      'visible' => array(
        ':input[name="method"]' => array(
          'value' => 'form',
        ),
      ),
      'required' => array(
        ':input[name="method"]' => array(
          'value' => 'form',
        ),
      ),
    ),
    '#weight' => 3,
  );

  // Path to file in directory.
  $form['import']['file_download'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to txt file', array(), $context),
    '#field_prefix' => 'default/files/',
    '#description' => t('Please write the full path to file. Path must include file name, which you would like to import.', array(), $context),
    '#states' => array(
      'enabled' => array(
        ':input[name="method"]' => array(
          'value' => 'file_download',
        ),
      ),
      'visible' => array(
        ':input[name="method"]' => array(
          'value' => 'file_download',
        ),
      ),
      'required' => array(
        ':input[name="method"]' => array(
          'value' => 'file_download',
        ),
      ),
    ),
    '#weight' => 4,
  );

  // File upload form.
  $form['import']['file'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload deploy txt file', array(), $context),
    '#description' => t('Only txt files are allowed.', array(), $context),
    '#upload_location' => 'public://deploy_import/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('txt'),
    ),
    '#states' => array(
      'enabled' => array(
        ':input[name="method"]' => array(
          'value' => 'file',
        ),
      ),
      'visible' => array(
        ':input[name="method"]' => array(
          'value' => 'file',
        ),
      ),
      'required' => array(
        ':input[name="method"]' => array(
          'value' => 'file',
        ),
      ),
    ),
    '#weight' => 5,
  );

  // Wrapper for action buttons.
  $form['import']['actions'] = array(
    '#type' => 'actions',
    '#weight' => 100,
  );

  // Submit button.
  $form['import']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import', array(), $context),
  );

  return $form;
}

/**
 * Validation handler for deploy_extra_import_entity_import_form().
 */
function deploy_extra_import_entity_import_form_validate($form, &$form_state) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_IMPORT_TRANSLATE_CONTEXT);

  // Get values.
  $method = $form_state['values']['method'];
  $data = '';

  // Invoke all methods.
  $methods = module_invoke_all('deploy_extra_formats_methods');

  // Define the import callback.
  $function = isset($methods[$method]) ? $methods[$method]['import_callback'] : '';

  // Call the import callback.
  if (function_exists($function)) {
    $data = $function($form_state['values']);
  }

  // Check data for emptiness.
  if (empty($data)) {
    form_set_error($method, t('You have empty imported data', array(), $context));
  }

  // Pass data to submission function.
  $form_state['deploy_extra_import_data'] = $data;
}

/**
 * Submission handler for deploy_extra_import_entity_import_form().
 */
function deploy_extra_import_entity_import_form_submit($form, &$form_state) {
  // Use in translation context.
  $context = array('context' => DEPLOY_EXTRA_IMPORT_TRANSLATE_CONTEXT);

  // Get values.
  $values = $form_state['values'];
  $format = $values['format'];
  $method = $values['method'];

  // Decode imported data.
  $data = deploy_extra_formats_convert_data($form_state['deploy_extra_import_data'], $format, 'decodeData');

  // Get lock name.
  $lock_name = !empty($data['processor_config']['lock_name']) ? $data['processor_config']['lock_name'] : NULL;

  // If have empty $lock_name, set the error and rebuild the form.
  if (!$lock_name) {
    form_set_error($method, t('Probably you have mixed format data to be imported or have tried to import incorrect data.', array(), $context));
    return $form;
  }

  // We allow only one import process of each plan at the time.
  if (!lock_acquire($lock_name)) {
    form_set_error($method, t('The import of @lock_name is already running.', array('@lock_name' => $lock_name), $context));
    return $form;
  }

  // Initialize array.
  $import_config = array();

  // Let other modules have their say.
  drupal_alter('deploy_extra_config_form', $form, $form_state, $import_config);

  // Default function name for import entity.
  $import_function = 'deploy_extra_import_import_entity';

  // If user choose additional options (lock, log, test mode) override import function.
  if (!empty($import_config) && function_exists('deploy_extra_import_entity')) {
    $import_function = 'deploy_extra_import_entity';
  }

  // Batch options.
  $batch_options = array(
    'finished' => 'deploy_extra_import_batch_finished_operation',
    'file' => drupal_get_path('module', 'deploy_extra_import') . '/deploy_extra_import.admin.inc',
  );

  // Get configured batch.
  $batch = deploy_extra_import_configure_batch($data, $import_function, $batch_options);

  batch_set($batch);
  batch_process('admin/structure/deploy/import');
}

/**
 * Form import callback.
 *
 * @param $values
 *   Values array after submission form.
 *
 * @return string
 *   Return encoded string.
 */
function deploy_extra_import_form_import($values) {
  return $values['form'];
}

/**
 * File for downloading import callback.
 *
 * @param $values
 *   Values array after submission form.
 *
 * @return string
 *   Return encoded string.
 */
function deploy_extra_import_file_download_import($values) {
  $path = $values['file_download'];
  $uri = 'public://' . $path;
  return file_exists($uri) ? file_get_contents($uri) : '';
}

/**
 * File import callback.
 *
 * @param $values
 *   Values array after submission form.
 *
 * @return string
 *   Return encoded string.
 */
function deploy_extra_import_file_import($values) {
  $fid = $values['file'];
  return !empty($fid) ? file_get_contents(file_load($fid)->uri) : '';
}
