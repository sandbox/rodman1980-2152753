api = 2
core = 7.x

projects[] = "drupal"

; Modules
projects[deploy][subdir] = "contrib"
projects[deploy][version] = "2.0-alpha2"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-alpha3"

projects[entity_dependency][type] = "module"
projects[entity_dependency][subdir] = "contrib"
projects[entity_dependency][download][type] = "git"
projects[entity_dependency][download][url] = "http://git.drupal.org/project/entity_dependency.git"
projects[entity_dependency][download][revision] = "7ca711a5149083ca2aa3b6ada9d35cd5abc6c99a"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[services][subdir] = "contrib"
projects[services][version] = "3.5"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.2"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.0-alpha5"
projects[uuid][patch][] = "http://drupal.org/files/uuid-uuid.core_.inc-2075325-2.patch"

projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.0-beta5"
projects[field_collection][patch][] = "http://drupal.org/files/field_collection-field_collection_uuid-2075325-2.patch"
